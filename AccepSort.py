import pandas
import os
import openpyxl
from time import strftime

dictionaryFilepath = ''
rebukesFilepath = 'rebukes.xlsx'
resultsFilepath = 'results.xlsx'
choise = 0


def intro():
    global choise
    print('\n Выберите действие:\n'
          '  1. Разделить замечания в rebukes.xlsx и создать results.xlsx\n'
          '  2. Расставить категории в results.xlsx по dictionary.xlsx\n'
          '  3. Последовательное выполнение действий 1 и 2.\n'
          '  4. Выход')
    choise = input('  Действие: ')
    if choise == '1':
        splitting()
        intro()
    elif choise == '2':
        choosedictionary()
        mapping()
        intro()
    elif choise == '3':
        choosedictionary()
        splitting()
        mapping()
        intro()
    elif choise == '4':
        raise SystemExit


def choosedictionary():
    global dictionaryFilepath
    dicfiles = []
    for dirpath, dirnames, filenames in os.walk('Словари'):
        for filename in filenames:
            if filename.split('.')[-1] == 'xlsx':
                dicfiles.append(str(''.join(list(os.path.join(dirpath, filename)))))
    if len(dicfiles) == 0:
        print('\n', 'Словари не найдены в папке "Словари".')
        input(' Press Enter to exit...')
        raise SystemExit
    print('\n', 'Выберите словарь:')
    dicid = 0
    for dic in dicfiles:
        dicid += 1
        dname = dic.split("\\")[-1]
        print(f'  {dicid}. {dname}')
    dicchoise = input('  Словарь: ')
    dictionaryFilepath = dicfiles[int(dicchoise) - 1]


def splitting():
    if os.path.isfile('results.xlsx'):
        try:
            os.remove('results.xlsx')
        except PermissionError:
            print(' Can not replace results.xlsx, file is opened.')
            input(' Press Enter to exit...')
            raise SystemExit
    print('\n', strftime("%H:%M:%S"), 'Разделение замечаний начато...')
    rebBook = openpyxl.open(rebukesFilepath, read_only=True, keep_vba=False)
    rebSheet = rebBook['Issue Management']
    resBook = openpyxl.Workbook()
    resSheet = resBook.active
    rebsize = rebSheet.max_row
    rebrowid = 0
    for row in rebSheet.iter_rows(max_col=23, max_row=rebsize, values_only=True):
        rebrowid += 1
        print(f' Выполнено [{rebrowid}/{rebsize}]', end='\r')
        rowaslist = list(row)
        rebukelist = row[5].splitlines()
        for rebuke in rebukelist:
            rowid = resSheet.max_row + 1
            colid = 0
            rowaslist[5] = rebuke
            for cell in rowaslist:
                colid += 1
                resSheet.cell(rowid, colid).value = cell
    resSheet.delete_rows(1, 1)  # удаление пустой строки из-за некорректности max_row на пустой странице
    print('\n', strftime("%H:%M:%S"), 'Сохранение файла...')
    resBook.save(resultsFilepath)
    print(strftime(" %H:%M:%S"), 'Разделение замечаний закончено.')


def mapping():
    global dictionaryFilepath
    print('\n', strftime("%H:%M:%S"), 'Расстановка категорий начата...')
    dic = pandas.read_excel(dictionaryFilepath, sheet_name=0, header=0, index_col=None)
    df = pandas.read_excel(resultsFilepath, sheet_name=0, header=0, index_col=None)
    df = df.replace(['\t\t\t\t\t\t\t', '\t\t\t\t\t\t', '\t\t\t\t\t', '\t\t\t\t', '\t\t\t', '\t\t', '\t'], '')
    df = df.replace('', pandas.NA)
    df = df.drop(df[df['Description'].isna()].index)
    df[dic.columns[3:].to_list()] = pandas.NA
    df[[dic.columns[1]]] = pandas.NA

    total = len(dic)
    for idx, row in dic.iterrows():
        print(f' Выполнено [{idx+1}/{total}]', end='\r')
        mask = (df['Description'].str.find(row.values[1]) != -1) & (df['Тип'].isna())
        df.loc[mask, dic.columns[1]] = row.values[1]
        df.loc[mask, dic.columns[3:].to_list()] = row.values[3:]

    noru = 'неотсортированные замечания'
    noen = 'unsorted comments'
    df.loc[df['Тип'].isna(), dic.columns[3:].to_list()] = [noru, noru, noru, noen, noen, noen]

    print('\n', strftime("%H:%M:%S"), 'Сохранение файла...')
    with pandas.ExcelWriter(resultsFilepath) as writer:
        df.to_excel(writer, sheet_name='Results', index=False, engine='openpyxl')

    print(strftime(" %H:%M:%S"), 'Расстановка категорий закончена.')


intro()
